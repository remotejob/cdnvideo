# cdnvideo.ML demo project


## WORKFLOW


## Connect git repository to Kubernetes cluster and create dedicated namespace
https://docs.gitlab.com/ee/user/clusters/agent/
```
helm upgrade --install kubeagent gitlab/gitlab-agent \
    --namespace cdnvideo-prod \
    --create-namespace \
    --set image.tag=v15.7.0-rc1 \
    --set config.token=************* \
    --set config.kasAddress=wss://kas.gitlab.com

```    
kubectl config set-context --current --namespace cdnvideo-prod

## Create SSL certificate (certifite will be upgraded automaticamente)
https://cert-manager.io/  
```
kubectl apply -f k8s/domain/cert-cdnvideo-ml.yaml  
kubectl get certificate  
kubectl get event  
kubectl get challenges
```

## Create DEV STAGING PROD enviroment **AUTOMATICAMENTE**
https://kustomize.io/

```
cd k8s/environments
kubectl kustomize overlays/dev > ../../manifest/dev/dev.yaml
kubectl kustomize overlays/staging > ../../manifest/stating/staging.yaml
kubectl kustomize overlays/prod > ../../manifest/prod/prod.yaml
```

## Deployment done by **GitOps** tecnology 
https://about.gitlab.com/topics/gitops/  
after any changes in manifest DIR related ENVIROMENT will be Upgraded **AUTOMATICAMENTE** 

## Persisten volume
k8s/environments/base/pvc.yaml  
(based on **longhorn DISTRIBUTED!** file system)  
https://longhorn.io/


## **Utils:** Copy static files to Persisten volume (simple deployment)
```
kubectl apply -f utils/pod_for_static_depl.yaml   
kubectl exec -it ubuntu -- /bin/bash  
kubectl cp  /mnt/seconddisk/repos/gitlab.com/remotejob/TapeScriptDemoSite/dist ubuntu:longhorndisk -c ubuntu  
kubectl delete -f utils/pod_for_static_depl.yaml
```
## Deployment for FRONTEND development can be done by CI/CD (using Docker,Kubernetes,NFS)
https://gitlab.com/remotejob/typescriptdemosite
